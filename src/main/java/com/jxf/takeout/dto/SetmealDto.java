package com.jxf.takeout.dto;

import com.jxf.takeout.entity.Setmeal;
import com.jxf.takeout.entity.SetmealDish;
import lombok.Data;
import java.util.List;

@Data
public class SetmealDto extends Setmeal {

    private List<SetmealDish> setmealDishes;

    private String categoryName;
}
