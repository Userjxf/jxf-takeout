package com.jxf.takeout.dto;

import com.jxf.takeout.entity.OrderDetail;
import com.jxf.takeout.entity.Orders;
import lombok.Data;
import java.util.List;

@Data
public class OrdersDto extends Orders {

    private String userName;

    private String phone;

    private String address;

    private String consignee;

    private List<OrderDetail> orderDetails;
	
}
