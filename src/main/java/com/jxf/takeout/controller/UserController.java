package com.jxf.takeout.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.jxf.takeout.common.R;
import com.jxf.takeout.entity.User;
import com.jxf.takeout.service.UserService;
import com.jxf.takeout.utils.SMSUtils;
import com.jxf.takeout.utils.ValidateCodeUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;
import java.util.Map;

@Slf4j
@RestController
@RequestMapping("user")
public class UserController {

    @Autowired
    private UserService userService;

    @PostMapping("/sendMsg")
    public R<String> sendMsg(@RequestBody User user, HttpSession session){
        String phone = user.getPhone();
        if (StringUtils.isNotBlank(phone)) {
            String code = ValidateCodeUtils.generateValidateCode(4).toString();
            log.info("code={}",code);
//            SMSUtils.sendMessage("外卖","",phone,code);
            session.setAttribute(phone,code);
            return R.success("手机验证码短信发送成功");
        }
        return R.error("手机验证码短信发送失败");
    }

//    @PostMapping("/login")
//    public R<User> login(@RequestBody Map map,HttpSession session){
//        log.info(map.toString());
//        String phone = map.get("phone").toString();
//        String code = map.get("code").toString();
//        Object codeInSession = session.getAttribute(phone);
//        if (codeInSession != null && codeInSession.equals(code)) {
//            LambdaQueryWrapper<User> lambdaQueryWrapper = new LambdaQueryWrapper<>();
//            lambdaQueryWrapper.eq(User::getPhone,phone);
//            User user = userService.getOne(lambdaQueryWrapper);
//            if (user == null){
//                user = new User();
//                user.setPhone(phone);
//                user.setStatus(1);
//                userService.save(user);
//            }
//            session.setAttribute("user",user.getId());
//            return R.success(user);
//        }
//        return R.error("登录失败");
//    }
    @PostMapping("/login")
    public R<User> login(@RequestBody User user,HttpSession session){
        String phone = user.getPhone();
        if (phone != null) {
            LambdaQueryWrapper<User> lambdaQueryWrapper = new LambdaQueryWrapper<>();
            lambdaQueryWrapper.eq(User::getPhone,phone);
            User user1 = userService.getOne(lambdaQueryWrapper);
            if (user1 == null){
                user1 = new User();
                user1.setPhone(phone);
                user1.setStatus(1);
                userService.save(user1);
            }
            session.setAttribute("user",user1.getId());
            return R.success(user1);
        }
        return R.error("登录失败");
    }
}
