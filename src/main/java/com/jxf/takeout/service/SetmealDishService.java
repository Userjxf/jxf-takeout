package com.jxf.takeout.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.jxf.takeout.entity.SetmealDish;

public interface SetmealDishService extends IService<SetmealDish> {
}
