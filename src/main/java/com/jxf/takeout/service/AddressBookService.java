package com.jxf.takeout.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.jxf.takeout.entity.AddressBook;

public interface AddressBookService extends IService<AddressBook> {
}
