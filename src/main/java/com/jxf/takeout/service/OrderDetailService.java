package com.jxf.takeout.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.jxf.takeout.entity.OrderDetail;

public interface OrderDetailService extends IService<OrderDetail> {
}
