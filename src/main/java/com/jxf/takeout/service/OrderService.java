package com.jxf.takeout.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.jxf.takeout.entity.Orders;

public interface OrderService extends IService<Orders> {
    public void submit(Orders orders);
}
