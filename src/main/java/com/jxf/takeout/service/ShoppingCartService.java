package com.jxf.takeout.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.jxf.takeout.entity.ShoppingCart;

public interface ShoppingCartService extends IService<ShoppingCart> {
}
