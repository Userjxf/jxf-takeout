package com.jxf.takeout.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.jxf.takeout.entity.DishFlavor;
import com.jxf.takeout.mapper.DishFlavorMapper;
import com.jxf.takeout.service.DishFlavorService;
import com.jxf.takeout.service.DishService;
import org.springframework.stereotype.Service;

@Service
public class DishFlavorServiceImpl extends ServiceImpl<DishFlavorMapper, DishFlavor> implements DishFlavorService {
}
