package com.jxf.takeout.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.jxf.takeout.entity.Employee;

public interface EmployeeService extends IService<Employee> {
}
