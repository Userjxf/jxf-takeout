package com.jxf.takeout.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.jxf.takeout.entity.Category;
import org.apache.ibatis.annotations.Param;

public interface CategoryService extends IService<Category> {
    void remove(Long id);
}
