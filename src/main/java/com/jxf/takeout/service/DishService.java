package com.jxf.takeout.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.jxf.takeout.dto.DishDto;
import com.jxf.takeout.entity.Dish;

public interface DishService extends IService<Dish> {

    public void saveWithFlavor(DishDto dishDto);

    public DishDto getByIdWithFlavor(Long id);

    public void updateWithFlavor(DishDto dishDto);

    public void deleteByIdWithFlavor(Long id);
}
