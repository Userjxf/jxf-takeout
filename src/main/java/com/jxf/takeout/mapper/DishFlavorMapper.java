package com.jxf.takeout.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jxf.takeout.entity.DishFlavor;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface DishFlavorMapper extends BaseMapper<DishFlavor> {
}
