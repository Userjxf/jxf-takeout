package com.jxf.takeout.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jxf.takeout.entity.Dish;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface DishMapper extends BaseMapper<Dish> {
}
