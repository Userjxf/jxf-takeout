package com.jxf.takeout.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jxf.takeout.entity.SetmealDish;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface SetmealDishMapper extends BaseMapper<SetmealDish> {
}
